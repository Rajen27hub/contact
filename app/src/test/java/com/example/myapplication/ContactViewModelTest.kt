package com.example.myapplication

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class ContactViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    private val firestore = mockk<FirebaseFirestore>()
    private val collectionReference = mockk<CollectionReference>()

    private lateinit var viewModel: ContactViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
        viewModel = ContactViewModel()
        mockkStatic(FirebaseFirestore::class)
        every { FirebaseFirestore.getInstance() } returns firestore
        every { firestore.collection(any()) } returns collectionReference
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
        unmockkAll()
    }

    @Test
    fun `test addContact`() = testScope.runBlockingTest {
        // Given
        val name = "Test Contact"
        val phoneNumbers = listOf("123-456-7890")
        val profileImage = null
        val bloodGroup = null
        val address = null

        coEvery { collectionReference.document(any()).set(any<Contact>()) } just runs

        // When
        viewModel.addContact(name, phoneNumbers, profileImage, bloodGroup, address)
        delay(1000)

        // Then
        coVerify { collectionReference.document(any()).set(any<Contact>()) }
    }

    @Test
    fun `test updateContact`() = testScope.runBlockingTest {
        // Given
        val contact = Contact("123", "Test Contact", listOf("123-456-7890"), null, null, null)

        coEvery { collectionReference.document(any()).set(any<Contact>()) } just runs

        // When
        viewModel.updateContact(contact)
        delay(1000)

        // Then
        coVerify { collectionReference.document(contact.id).set(contact) }
    }

    @Test
    fun `test deleteContact`() = testScope.runBlockingTest {
        // Given
        val contact = Contact("123", "Test Contact", listOf("123-456-7890"), null, null, null)

        coEvery { collectionReference.document(any()).delete() } just runs

        // When
        viewModel.deleteContact(contact)
        delay(1000)

        // Then
        coVerify { collectionReference.document(contact.id).delete() }
    }

}

infix fun Any.just(runs: Runs) {
    mockk<() -> Unit>().also { fn ->
        every { fn.invoke() } answers { runs }
//        this.mockedCalls += MockedCall(fn, emptyArray())
    }
}

