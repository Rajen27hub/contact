package com.example.myapplication

data class Contact(
    var id: String = "",
    var name: String = "",
    var phoneNumbers: List<String> = emptyList(),
    var profileImage: String? = null,
    var bloodGroup: String? = null,
    var address: String? = null
)

