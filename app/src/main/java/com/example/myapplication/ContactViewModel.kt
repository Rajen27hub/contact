package com.example.myapplication

import android.util.Log
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class ContactViewModel : ViewModel() {

    private val firestore = Firebase.firestore
    private val contactCollectionRef = firestore.collection("contacts")

    private val _contacts = MutableStateFlow<List<Contact>>(emptyList())
    val contacts: StateFlow<List<Contact>> = _contacts

    private val _currentContact = MutableStateFlow<Contact?>(null)
    val currentContact: StateFlow<Contact?> = _currentContact

    init {
        // Listen for changes to the contacts collection in Firestore
        contactCollectionRef.addSnapshotListener { snapshot, error ->
            if (error != null) {
                Log.e(TAG, "Error getting contacts: $error")
                return@addSnapshotListener
            }

            val contacts = snapshot?.documents?.mapNotNull {
                it.toObject(Contact::class.java)
            } ?: emptyList()

            _contacts.value = contacts
        }
    }

    fun selectContact(contact: Contact) {
        _currentContact.value = contact
    }

    fun deselectContact() {
        _currentContact.value = null
    }

    fun addContact(name: String, phoneNumbers: List<String>, profileImage: String?, bloodGroup: String?, address: String?) {
        val id = contactCollectionRef.document().id
        val contact = Contact(id, name, phoneNumbers, profileImage, bloodGroup, address)
        contactCollectionRef.document(id).set(contact)
    }

    fun updateContact(contact: Contact) {
        contactCollectionRef.document(contact.id).set(contact)
    }

    fun deleteContact(contact: Contact) {
        contactCollectionRef.document(contact.id).delete()
    }

    companion object {
        private const val TAG = "ContactViewModel"
    }
}