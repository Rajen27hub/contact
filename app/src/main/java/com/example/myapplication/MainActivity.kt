package com.example.myapplication

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.myapplication.ui.theme.MyApplicationTheme

class MainActivity : ComponentActivity() {
    private val viewModel: ContactViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ContactListScreen(
                viewModel = viewModel,
                onContactSelected = { contact ->
                    viewModel.selectContact(contact)
                }
            )
        }
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ContactListScreen(
    viewModel: ContactViewModel,
    onContactSelected: (Contact) -> Unit
) {
    val contacts by viewModel.contacts.collectAsState()

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Contacts") }
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = { onContactSelected(Contact("", "", emptyList(), null, null, null)) }
            ) {
                Icon(Icons.Filled.Add, contentDescription = null)
            }
        }
    ) {
        ContactList(
            contacts = contacts,
            onContactClick = { viewModel.selectContact(it) },
            onContactDelete = { viewModel.deleteContact(it) }
        )
    }

    val currentContact by viewModel.currentContact.collectAsState()

    if (currentContact != null) {
        ContactDetailScreen(
            contact = currentContact,
            onClose = { viewModel.deselectContact() },
            onSave = {
                if (currentContact!!.id.isBlank()) {
                    viewModel.addContact(it.name, it.phoneNumbers, it.profileImage, it.bloodGroup, it.address)
                } else {
                    viewModel.updateContact(it)
                }
                viewModel.deselectContact()
            },
            onDelete = { viewModel.deleteContact(currentContact!!) }
        )
    }
}

@Composable
fun ContactList(
    contacts: List<Contact>,
    onContactClick: (Contact) -> Unit,
    onContactDelete: (Contact) -> Unit
) {
    LazyColumn {
        items(contacts) { contact ->
            ContactItem(
                contact = contact,
                onClick = { onContactClick(contact) },
                onDelete = { onContactDelete(contact) }
            )
        }
    }
}

@Composable
fun ContactItem(
    contact: Contact,
    onClick: () -> Unit,
    onDelete: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .clickable { onClick() },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = rememberImagePainter(contact.profileImage),
            contentDescription = "Profile Image",
            modifier = Modifier
                .size(50.dp)
                .clip(CircleShape)
        )
        Spacer(modifier = Modifier.width(16.dp))
        Column {
            Text(text = contact.name, fontWeight = FontWeight.Bold)
            Text(text = contact.phoneNumbers.joinToString(", "))
        }
        IconButton(onClick = onDelete) {
            Icon(Icons.Default.Delete, contentDescription = "Delete Contact")
        }
    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ContactDetailScreen(
    contact: Contact?,
    onClose: () -> Unit,
    onSave: (Contact) -> Unit,
    onDelete: () -> Unit
) {
    val name = remember { mutableStateOf(contact?.name ?: "") }
    val phoneNumbers = remember { mutableStateOf(contact?.phoneNumbers ?: emptyList()) }
    val profileImage = remember { mutableStateOf(contact?.profileImage) }
    val bloodGroup = remember { mutableStateOf(contact?.bloodGroup) }
    val address = remember { mutableStateOf(contact?.address) }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Contact Details") },
                navigationIcon = {
                    IconButton(onClick = onClose) {
                        Icon(Icons.Filled.ArrowBack, contentDescription = "Back")
                    }
                },
                actions = {
                    if (contact != null) {
                        IconButton(onClick = onDelete) {
                            Icon(Icons.Filled.Delete, contentDescription = "Delete")
                        }
                    }
                }
            )
        },
        content = {
            Column(modifier = Modifier.padding(16.dp)) {
                OutlinedTextField(
                    value = name.value,
                    onValueChange = { name.value = it },
                    label = { Text(text = "Name") },
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(16.dp))
                PhoneNumbersField(
                    phoneNumbers = phoneNumbers.value,
                    onPhoneNumbersChange = { phoneNumbers.value = it },
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(16.dp))
                ImageField(
                    imageUri = profileImage.value,
                    onImageUriChange = { profileImage.value = it },
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(16.dp))
                BloodGroupField(
                    bloodGroup = bloodGroup.value,
                    onBloodGroupChange = { bloodGroup.value = it },
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(16.dp))
                AddressField(
                    address = address.value?:"",
                    onAddressChange = { address.value = it },
                    modifier = Modifier.fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(16.dp))
                Button(
                    onClick = { onSave(Contact(contact?.id ?: "", name.value, phoneNumbers.value, profileImage.value, bloodGroup.value, address.value)) },
                    modifier = Modifier.align(Alignment.End)
                ) {
                    Text(text = "Save")
                }
            }
        }
    )
}


@Composable
fun PhoneNumbersField(
    phoneNumbers: List<String>,
    onPhoneNumbersChange: (List<String>) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier) {
        Text(text = "Phone Numbers")
        Spacer(modifier = Modifier.height(8.dp))
        phoneNumbers.forEachIndexed { index, phoneNumber ->
            OutlinedTextField(
                value = phoneNumber,
                onValueChange = { updatedPhoneNumber ->
                    val updatedPhoneNumbers = phoneNumbers.toMutableList().apply {
                        set(index, updatedPhoneNumber)
                    }
                    onPhoneNumbersChange(updatedPhoneNumbers)
                },
                label = { Text(text = "Phone Number ${index + 1}") },
                modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(8.dp))
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            IconButton(onClick = {
                val updatedPhoneNumbers = phoneNumbers.toMutableList().apply {
                    add("")
                }
                onPhoneNumbersChange(updatedPhoneNumbers)
            }) {
                Icon(Icons.Filled.Add, contentDescription = "Add phone number")
            }
            TextButton(onClick = {
                val updatedPhoneNumbers = phoneNumbers.toMutableList().apply {
                    if (size > 1) {
                        removeLast()
                    }
                }
                onPhoneNumbersChange(updatedPhoneNumbers)
            }) {
                Text(text = "Remove phone number")
            }
        }
    }
}

@Composable
fun ImageField(
    imageUri: String?,
    onImageUriChange: (String?) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier) {
        Text(text = "Profile Image")
        Spacer(modifier = Modifier.height(8.dp))
        if (imageUri != null) {
            Image(
                painter = rememberImagePainter(data = imageUri),
                contentDescription = "Profile image",
                modifier = Modifier.size(120.dp).clip(CircleShape)
            )
        } else {
            Box(
                modifier = Modifier.size(120.dp).clip(CircleShape)
                    .background(MaterialTheme.colors.surface)
            ) {
                Icon(
                    Icons.Filled.Person,
                    contentDescription = "No profile image",
                    modifier = Modifier.align(Alignment.Center)
                )
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        TextButton(onClick = {
            // Launch a file picker to select an image
            onImageUriChange("https://picsum.photos/id/${(1..100).random()}/200/200")
        }) {
            Text(text = "Change profile image")
        }
    }
}

@Composable
fun BloodGroupField(
    bloodGroup: String?,
    onBloodGroupChange: (String?) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier) {
        Text(text = "Blood Group")
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = bloodGroup ?: "",
            onValueChange = { onBloodGroupChange(it.takeIf { it.isNotBlank() }) },
            label = { Text(text = "Blood Group") },
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
fun AddressField(address: String, onAddressChange: (String) -> Unit, modifier: Modifier = Modifier) {
    OutlinedTextField(
        value = address,
        onValueChange = onAddressChange,
        label = { Text(text = "Address") },
        modifier = modifier
    )
}